**/api/schedule**

_RESPONSE_

POST

Content-type: application/json

Body:


	
	{
	    "data": {
		    "station": "SVO",
		    "type":"arrival"
	    }
	}
	
	or
	
	    {
    	    "data": {
    		    "station": "SVO",
    		    "type":"flight-search"
    		    "flight": "UT-123"
    	    }
    
        }

* type: arrival, departure, delay, flight-search

_ANSWER_

 Status: **200**

	 [
	        {    
                "arrival": null,
                "title": "Москва — Новосибирск",
                "number": "5N 237",
                "carrier": {
                    "title": "Нордавиа"
                },
                "vehicle": "Boeing 737-800",
                "is_fuzzy": false,
                "departure": "2019-10-21T00:05:00+03:00",
                "terminal": null,
	        }
	 ]
