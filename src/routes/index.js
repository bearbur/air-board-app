const express = require('express');
// eslint-disable-next-line
const router = express.Router();
const schedule = require('../helpers/schedule');
const path = require('path');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});

router.get('/', function(req, res, next) {
    res.sendFile(path.join(__dirname + '/dist/index.html'));
});

/* POST schedule request. */
router.post('/api/schedule', function(req, res, next) {
    // todo function (req.body)
    // request to Yandex API: apikey, station: IATA, lang: RU,
    // transport_types: plane, event: departure/arrival, system: iata, show_systems: all

    schedule(req.body)
        .then(r => {
            res.status(200).json(r);
        })
        .catch(e => {
            // eslint-disable-next-line
            console.error(e);
            res.status(404).json({ error: 'api not ready' });
        });
});

module.exports = router;
