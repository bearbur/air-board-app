import React from 'react';
import ReactDOM from 'react-dom';
import Board from './components/board';
import { AppWrapper, AppTitle } from './styles/styled-app';
import { Labels } from './constants';

const App = () => {
    return (
        <AppWrapper>
            <AppTitle>{Labels.AppTitle}</AppTitle>
            <Board />
        </AppWrapper>
    );
};

ReactDOM.render(<App />, document.getElementById('root'));
