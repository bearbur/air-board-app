export const eventTypes = [
    {
        value: 'ПРИЛЕТ',
        eventType: 'arrival'
    },
    {
        value: 'ВЫЛЕТ',
        eventType: 'departure'
    },
    {
        value: 'ЗАДЕРЖАН',
        eventType: 'delay'
    }
];

export const Labels = {
    Airport: 'АЭРОПОРТ',
    AppTitle: 'ТАБЛО АЭРОПОРТА',
    FindByCode: 'искать по коду рейса',
    CodePlaceholder: 'код рейса',
    Types: '',
    Buttons: {
        Find: 'ПОКАЗАТЬ',
        FindFlight: 'ИСКАТЬ'
    }
};

const baseUrl = 'http://localhost:7777';

export const urls = {
    schedule: `${baseUrl}/api/schedule`
};

export const messages = {
    notRequestedYet: `${Labels.Buttons.Find} / введите ${Labels.CodePlaceholder} и  ${Labels.Buttons.FindFlight}.`
};
