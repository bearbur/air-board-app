import React from 'react';
import { InformationBoardWrapper, InformationBoardWrapperList } from '../styles/styled-scheldue-board';
import ScheldueBoardEventRow from './scheldue-event-row';
import { messages } from '../constants';

const ScheldueBoard = ({ airportEvents }) => {
    return (
        <InformationBoardWrapper>
            <InformationBoardWrapperList>
                {airportEvents.map((event, key) => (
                    <ScheldueBoardEventRow key={key} event={event} />
                ))}
            </InformationBoardWrapperList>
            {airportEvents.length === 0 && <span>Кликните по кнопке {messages.notRequestedYet}</span>}
        </InformationBoardWrapper>
    );
};

export default ScheldueBoard;
