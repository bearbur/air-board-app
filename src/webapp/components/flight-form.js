import React, { useEffect, useState } from 'react';
import {
    FlightFormWrapper,
    FlightTitle,
    FlightInputWrapper,
    FlightInput,
    FlightSearchButton
} from '../styles/styled-flight-form';
import { Labels, urls } from '../constants';
import axios from 'axios';

const FlightForm = ({ setAirportEvents, setWarning, station }) => {
    const [flight, setFlight] = useState('');
    const [requestIsProcessing, setRequestIsProcessing] = useState(false);

    useEffect(() => {
        if (!station) {
            setWarning('Пожалуйста, укажите аэропорт в международном формате (например для Домодедово: DME).');
        }
        if (requestIsProcessing && station) {
            const requestObject = {
                method: 'post',
                url: urls.schedule,
                data: {
                    data: {
                        station,
                        flight
                    }
                }
            };
            axios(requestObject)
                .then(r => {
                    setRequestIsProcessing(false);
                    if (r.status === 200) {
                        setAirportEvents(r.data);
                    } else {
                        throw new Error(r);
                    }
                })
                .catch(e => {
                    // eslint-disable-next-line
                    console.error(e);
                    setWarning('Ошибка при обращении к серверу.');
                    setRequestIsProcessing(false);
                });
        } else {
            setRequestIsProcessing(false);
        }
    }, [requestIsProcessing]);

    const handleRequestInformation = e => {
        e.preventDefault();
        setRequestIsProcessing(true);
    };

    return (
        <FlightFormWrapper>
            <FlightTitle>{Labels.FindByCode}</FlightTitle>
            <FlightInputWrapper>
                <FlightInput
                    type="input"
                    placeholder={Labels.CodePlaceholder}
                    onChange={e => setFlight(e.target.value.toUpperCase())}
                    value={flight}
                />
                <FlightSearchButton
                    type="button"
                    value={Labels.Buttons.FindFlight}
                    onClick={handleRequestInformation}
                />
            </FlightInputWrapper>
        </FlightFormWrapper>
    );
};

export default FlightForm;
