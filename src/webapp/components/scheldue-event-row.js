import React from 'react';
import { ScheldueBoardEventRowItem, ScheldueBoardEventRowWrapper } from '../styles/styled-scheldue-board';

const ScheldueBoardEventRow = ({ event }) => {
    return (
        <ScheldueBoardEventRowWrapper>
            {Object.values(event)
                .filter(r => {
                    if (r) {
                        return true;
                    } else {
                        return false;
                    }
                })
                .map((item, key) => (
                    <ScheldueBoardEventRowItem entity={item.toString()} key={key}>
                        {item.toString()}
                    </ScheldueBoardEventRowItem>
                ))}
        </ScheldueBoardEventRowWrapper>
    );
};

export default ScheldueBoardEventRow;
