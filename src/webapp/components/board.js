import React, { useState } from 'react';
import ScheldueForm from './scheldue-form';
import ScheldueBoard from './scheldue-board';
import { BoardWrapper, FlightFinderWrapper, ScheldueWrapper } from '../styles/styled-board';
import FlightForm from './flight-form';
import InfoForUser from './info-for-user';

const Board = () => {
    const [airportEvents, setAirportEvents] = useState([]);
    const [warning, setWarning] = useState('');
    const [station, setStation] = useState('DME');
    const [eventType, setEventStatus] = useState('departure');

    return (
        <BoardWrapper>
            <ScheldueWrapper>
                <ScheldueForm
                    setAirportEvents={setAirportEvents}
                    setWarning={setWarning}
                    setStation={setStation}
                    station={station}
                    eventType={eventType}
                    setEventStatus={setEventStatus}
                />
                <ScheldueBoard airportEvents={airportEvents} />
            </ScheldueWrapper>
            <FlightFinderWrapper>
                <FlightForm setAirportEvents={setAirportEvents} setWarning={setWarning} station={station} />
            </FlightFinderWrapper>
            {warning && <InfoForUser warning={warning} setWarning={setWarning} />}
        </BoardWrapper>
    );
};

export default Board;
