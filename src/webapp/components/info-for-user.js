import React from 'react';
import { InfoForUserWrapper } from '../styles/styled-info-for-user';

const InfoForUser = ({ warning, setWarning }) => {
    return (
        <InfoForUserWrapper>
            <span>{warning}</span>
            <button onClick={() => setWarning('')}>закрыть</button>
        </InfoForUserWrapper>
    );
};

export default InfoForUser;
