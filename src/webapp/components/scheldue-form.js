import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {
    LabelsForRequestParamsAtForm,
    LabelsForRequestParamsAtFormType,
    RequestInformationFormWrapper,
    RowFormInputWrapper,
    StationInput,
    TypeOfEventsButton,
    RequestInformationButton
} from '../styles/styled-scheldue-form';
import { TypeOfEventsButtonWrapper } from '../styles/styled-scheldue-board';
import { Labels, eventTypes, urls } from '../constants';

const ScheldueForm = ({ setAirportEvents, setWarning, station, setStation, eventType, setEventStatus }) => {
    const [requestIsProcessing, setRequestIsProcessing] = useState(false);

    useEffect(() => {
        if (!station) {
            setWarning('Пожалуйста, укажите аэропорт в международном формате (например для Домодедово: DME).');
        }
        if (requestIsProcessing && station) {
            const requestObject = {
                method: 'post',
                url: urls.schedule,
                data: {
                    data: {
                        station,
                        event: eventType
                    }
                }
            };
            axios(requestObject)
                .then(r => {
                    setRequestIsProcessing(false);
                    if (r.status === 200) {
                        setAirportEvents(r.data);
                    } else {
                        throw new Error(r);
                    }
                })
                .catch(e => {
                    // eslint-disable-next-line
                    console.error(e);
                    setWarning('Ошибка при обращении к серверу.');
                    setRequestIsProcessing(false);
                });
        } else {
            setRequestIsProcessing(false);
        }
    }, [requestIsProcessing]);

    const handleStatusOfEvents = e => {
        setEventStatus(e.target.name);
    };

    const handleRequestInformation = e => {
        e.preventDefault();
        setRequestIsProcessing(true);
    };

    return (
        <RequestInformationFormWrapper>
            <RowFormInputWrapper>
                <LabelsForRequestParamsAtForm>{Labels.Airport}</LabelsForRequestParamsAtForm>
                <StationInput
                    value={station}
                    type="input"
                    placeholder="Код аэропорта ( например DME)"
                    onChange={e => setStation(e.target.value.toUpperCase())}
                />
            </RowFormInputWrapper>
            <RowFormInputWrapper>
                <LabelsForRequestParamsAtFormType>{Labels.Types}</LabelsForRequestParamsAtFormType>
                <TypeOfEventsButtonWrapper>
                    {eventTypes.map((eventInfoTyp, key) => (
                        <TypeOfEventsButton
                            key={key}
                            value={eventInfoTyp.value}
                            name={eventInfoTyp['eventType']}
                            type="button"
                            isActive={eventType === eventInfoTyp['eventType']}
                            onClick={handleStatusOfEvents}
                        />
                    ))}
                </TypeOfEventsButtonWrapper>
            </RowFormInputWrapper>
            <RowFormInputWrapper>
                {!requestIsProcessing && (
                    <RequestInformationButton
                        value={Labels.Buttons.Find}
                        type="button"
                        onClick={handleRequestInformation}
                    />
                )}
            </RowFormInputWrapper>
        </RequestInformationFormWrapper>
    );
};

export default ScheldueForm;
