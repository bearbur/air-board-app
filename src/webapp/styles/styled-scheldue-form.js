import styled from 'styled-components';

export const RequestInformationFormWrapper = styled.div`
    min-height: 90px;
    min-width: 200px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    background: rgba(101, 10, 23, 1);
`;

export const StationInput = styled.input`
    background: grey;
    color: purple;
    min-width: 18em;
    text-align: center;
    border: none;
    background: #ddd;
`;

export const RowFormInputWrapper = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    min-width: 100%;
    min-height: 1.5em;
`;

export const LabelsForRequestParamsAtForm = styled.span`
    background: #ddd;
    min-width: 5em;
    max-width: 20%;
    text-align: right;
    padding-right: 2em;
`;

export const LabelsForRequestParamsAtFormType = styled(LabelsForRequestParamsAtForm)`
    background: rgba(101, 10, 23, 1);
    max-width: 100%;
`;

export const TypeOfEventsButton = styled.input`
    background: ${props => (props.isActive ? 'rgba(0, 250, 0, 1)' : 'rgba(0, 0, 0, 0.5)')};
    color: ${props => (props.isActive ? 'rgba(0, 0, 0, 1)' : 'rgba(0, 250, 255, 1)')};
    min-width: 6em;
    padding: 0;
    border: none;
    height: 2em;
`;

export const RequestInformationButton = styled.input`
    background-image: linear-gradient(to bottom right, rgba(101, 11, 23, 1), rgba(101, 11, 23, 1));
    border: none;
    color: rgba(234, 230, 226, 1);
    width: 100%;
    letter-spacing: 0em;
    font-weight: 400;
    text-align: left;
    padding-right: 3em;
    font-size: 1.2em;
    :hover {
        background-image: linear-gradient(to top right, rgba(90, 20, 50, 1), rgba(101, 11, 23, 1));
    }
`;
