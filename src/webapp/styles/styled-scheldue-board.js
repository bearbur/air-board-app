import styled from 'styled-components';

export const InformationBoardWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 90vw;
    min-height: 200px;
    padding: 3em;
`;

export const InformationBoardWrapperList = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    min-width: 100%;
    min-height: 50vh;
`;

export const TypeOfEventsButtonWrapper = styled.div`
    padding: 0;
    margin: 0;
`;

export const ScheldueBoardEventRowWrapper = styled.div`
    max-width: 100%;
    min-width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: space-around;
    max-height: 4em;
    overflow: auto;
    :hover {
        background: chocolate;
    }
`;

export const ScheldueBoardEventRowItem = styled.span`
    max-width: 14%;
    min-width: 14%;
    min-height: 100%;
    font-size: 0.8em;
    background: ${props => propsBackGround(props.entity)};
    text-align: center;
    overflow-y: auto;
`;

const propsBackGround = item => {
    if (item === 'delay') {
        return 'rgb(255, 153, 51, 0.5)';
    }
    if (item === 'canceled') {
        return 'rgb(204, 0, 0, 0.5)';
    }
    if (item === 'finished') {
        return 'rgb(0, 153, 0, 0.5)';
    }
    if (item === 'service') {
        return 'rgb(102, 153, 153, 0.5)';
    }
    if (item === 'in_progress') {
        return 'rgb(51, 51, 204, 0.5)';
    }
};
