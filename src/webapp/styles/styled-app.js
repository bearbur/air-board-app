import styled from 'styled-components';

export const AppWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    background: rgba(200, 200, 200, 0.1);
    padding: 0;
    max-height: 100vh;
    min-height: 100vh;
    overflow-y: auto;
`;

export const AppTitle = styled.span`
    font-weight: 400;
    font-size: 1.5em;
    color: rgba(234, 230, 226, 1);
    background: rgba(101, 10, 23, 1);
    padding-top: 0.5em;
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: flex-start;
    width: 95%;
    min-height: 30px;
`;
