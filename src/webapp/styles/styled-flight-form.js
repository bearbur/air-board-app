import styled from 'styled-components';

export const FlightFormWrapper = styled.div`
    min-height: 90px;
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: flex-start;
    background: rgba(101, 10, 23, 1);
`;

export const FlightTitle = styled.span`
    color: rgba(234, 230, 226, 1);
    min-height: 1em;
    width: 90%;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    text-align: right;
`;

export const FlightInputWrapper = styled.div`
    min-height: 1em;
    width: 90%;
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
`;

export const FlightInput = styled.input`
    max-width: 60%;
    border: none;
    text-align: center;
`;

export const FlightSearchButton = styled.input`
    max-width: 40%;
    min-width: 50px;
    border: none;
    background: rgba(0, 250, 0, 1);
    color: rgba(123, 20, 126, 1);
    font-weight: bold;
    font-size: 1em;
`;
