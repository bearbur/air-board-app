import styled from 'styled-components';

export const InfoForUserWrapper = styled.div`
    position: fixed;
    top: 10em;
    right: 45vw;
    width: 10em;
    height: 10em;
    border: 1px solid black;
    border-radius: 10px;
`;
