import styled from 'styled-components';

export const BoardWrapper = styled.div`
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    width: 95%;
    min-height: 420px;
`;

export const ScheldueWrapper = styled.div`
    max-width: 65%;
    min-width: 65%;
    min-height: 420px;
`;

export const FlightFinderWrapper = styled.div`
    max-width: 35%;
    min-width: 35%;
    min-height: 90px;
`;
