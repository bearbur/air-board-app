const StatusOfFlightService = sheduleToUser => {
    const cloneOfData = sheduleToUser.slice();

    return cloneOfData.map(element => {
        return Object.assign({}, element, fakeStatusRequester());
    });
};

const fakeStatusRequester = () => {
    const random = Math.round(100 * Math.random());

    if (random < 100 && random >= 90) {
        return { status: 'delay' };
    }
    if (random < 90 && random >= 50) {
        return { status: 'in_progress' };
    }
    if (random < 50 && random >= 15) {
        return { status: 'finished' };
    }
    if (random < 15 && random >= 12) {
        return { status: 'canceled' };
    }
    if (random < 12) {
        return { status: 'service' };
    }
    return { status: 'default' };
};

module.exports = {
    StatusOfFlightService: StatusOfFlightService
};
