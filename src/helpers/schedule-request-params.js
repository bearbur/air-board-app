const limit = 1000;
const requestParams = body => {
    let d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    const date = `${year}-${month}-${day}`;
    let customRequest = `?system=iata&transport_types=plane&limit=${limit}&date=${date}`;
    if (body.hasOwnProperty('data')) {
        const data = Object.assign({}, body.data);
        for (let key in data) {
            if (data.hasOwnProperty(key) && data[key]) {
                if (data[key] === 'arrival' || data[key] === 'departure' || key === 'station') {
                    customRequest = customRequest + '&' + key + '=' + data[key];
                }
            }
        }
        return customRequest;
    } else {
        // eslint-disable-next-line
        console.error('Not find data object in request.');
        return null;
    }
};

module.exports = {
    requestParams: requestParams
};
