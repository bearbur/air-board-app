const request = require('request');
const yandexAPI = require('../config/yandex-api');
const backers = require('./schedule-request-params');
const ScheduleResponseTransformer = require('./schedule-response-transformer');

const schedule = body => {
    const requestParams = backers.requestParams(body);
    return new Promise((resolve, reject) => {
        if (!requestParams) {
            reject(new Error('Empty request params.'));
        }
        const urlToAPI = yandexAPI.urls.schedule + requestParams;
        request.get(
            urlToAPI,
            {
                headers: {
                    Authorization: yandexAPI.token
                }
            },
            (err, resp) => {
                if (err || !resp) {
                    // eslint-disable-next-line
                    console.error(err);
                    reject(new Error('Error on request to API.'));
                }

                const responseRaw = Object.assign({}, JSON.parse(resp.body));

                if (!responseRaw) {
                    reject(new Error('Error on request to API.'));
                }
                //todo transform response here

                let response = ScheduleResponseTransformer(responseRaw, Object.assign({}, body));

                resolve(response);
            }
        );
    });
};

module.exports = schedule;
