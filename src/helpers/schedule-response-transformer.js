const { StatusOfFlightService } = require('./status-of-flight-service');
const ScheduleResponseTransformer = (responseRaw, body) => {
    const scheduleArray = responseRaw.schedule.slice();
    const event = body.data.event;
    const flight = body.data.flight;

    let sheduleToUser;

    if (!flight) {
        sheduleToUser = scheduleArray.map(event => ({
            arrival: event.arrival,
            title: event['thread']['title'],
            number: event['thread']['number'],
            'carrier-title': event['thread']['carrier']['title'],
            vehicle: event['thread']['venicle'],
            is_fuzzy: event['iz_fuzzy'],
            departure: event['departure'],
            terminal: event['terminal']
        }));
    }

    if (flight) {
        const flightInfo = scheduleArray.find(element => {
            return element['thread']['number'].includes(flight);
        });

        sheduleToUser = [
            {
                arrival: flightInfo.arrival,
                title: flightInfo['thread']['title'],
                number: flightInfo['thread']['number'],
                'carrier-title': flightInfo['thread']['carrier']['title'],
                vehicle: flightInfo['thread']['venicle'],
                is_fuzzy: flightInfo['iz_fuzzy'],
                departure: flightInfo['departure'],
                terminal: flightInfo['terminal']
            }
        ];
    }

    const rawData = StatusOfFlightService(sheduleToUser);

    if (event === 'delay') {
        return rawData.filter(event => event.status === 'delay');
    }

    return rawData;
};

module.exports = ScheduleResponseTransformer;
